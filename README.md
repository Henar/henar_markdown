# Instalación de la apliación
Para instalar la app...

# Configuración de la aplicación
## Instrucciones
1. Entras es github, le das a new proyect, pones el nombre del proyecto y creas el proyecto.  
![imagen](Imagenes Actividad1/Uno.JPG "Crear proyecto en git")   
2. Se clona el repositorio de github, copiando la dirección del enlace que te aparece.  
![imagen](Imagenes Actividad1/Dos.JPG "Clonar el proyecto de git")  
  * Se puede usar las instrucciones que aparece en GitLab al crear el repositorio, eligiendo la opción adecuada  
![imagen](Imagenes Actividad1/DosA.JPG "Opciones de GitHub")    
3. Hacer el primer commit y subirlo  
![imagen](Imagenes Actividad1/Tres.JPG "Primer commit y push")   
4. Crear el fichero privado.txt  
![imagen](Imagenes Actividad1/SeisA.JPG "Crear fichero")  ![imagen](Imagenes Actividad1/SeisB.JPG "Crear fichero")  
5. Crear la carpeta privada  
![imagen](Imagenes Actividad1/SeisC.JPG "Crear carpeta")  ![imagen](Imagenes Actividad1/SeisD.JPG "Crear carpeta")  
6. Creas un fichero .gitignore  
![imagen](Imagenes Actividad1/SieteA.JPG "Crear fichero .gitignore")  
7. Añades en el fichero lo que quieres que se omita  
![imagen](Imagenes Actividad1/SieteB.JPG "Añadiendo ficheros y directorios a omitir")  
8. Creo el fichero y añado las asignaturas  
 ![imagen](Imagenes Actividad1/OchoA.JPG "Creación fichero")  ![imagen](Imagenes Actividad1/OchoB.JPG "Modificando el fichero")  
9. Creo una etiqueta   
![imagen](Imagenes Actividad1/Nueve.JPG "Creación de una etiqueta")  
10. Subir los cambios al repositorio remoto  
![imagen](Imagenes Actividad1/Diez.JPG "Subir los cambios al repositorio")   




## Comandos
|		Comando 	 	 		|			  Uso			  			 |
|:-----------------------------:|:--------------------------------------:|
|				cd		 		|Para ubicarnos en la carpeta que queremos que se copie|
|		 git clone		 		|Para clonar el proyecto de GitHub 		 |
|	 git init (No hace falta)   |Se inicializa el repositorio 			 |
|      git status               | Para ver los cambios que se han hecho y donde se encuentran| 
|			git add	.	 		|Para añadir los cambios al index        |
|git commit -m "nombre commit"	|Para añadir los cambios al head con ese nombre para identificarle|
| git push -u origin master	   	|Para subir los cambios a la rama principal|
|   git tag nombre				| Para crear una etiqueta				 |

***
##### Enlaces de compañeros    
| Nombre 				| Enlace 				|
|----------------------:|:----------------------|
|Yesica Rábago			|[Enlace al repositorio de Yesica](https://gitlab.com/Yesica.RL/yesica_markdown.git, "Repositorio de Yesica")|

***
***
***
# Hoja03_Markdown_03  
## Creación de ramas  
1. Creo la rama con mi nombre  
![imagen](Imagenes Actividad2/uno.jpg "Creación de la rama")   
2. Me posiciono en la rama creada  
![imagen](Imagenes Actividad2/dos.jpg "Me muevo de rama")  

## Añado un fichero y creo la rama remota  
3. Creo un fichero   
![imagen](Imagenes Actividad2/tresa.jpg "Creo el fichero")   ![imagen](Imagenes Actividad2/tresb.jpg "Edito el fichero")  
4. Hago un commit    
![imagen](Imagenes Actividad2/cuatro.jpg "Commit")   
5. Subir los cambios    
![imagen](Imagenes Actividad2/cinco.jpg "Push")   

### Hacer merge directo  
6. Posiciono en la rama master      
![imagen](Imagenes Actividad2/seis.jpg "Posicionamiento")
7. Hacer merge de la rama creada con la actual    
![imagen](Imagenes Actividad2/siete.jpg "Merge mal")
![imagen](Imagenes Actividad2/sieteb.jpg "Merge")

### Merge con conflicto  
8. Añadir al fichero daw.md una tabla    
![imagen](Imagenes Actividad2/ochoa.jpg "Cambiar a rama master")  
![imagen](Imagenes Actividad2/ochob.jpg "Cambios")  
9. Añadir los cambios y hacer un commit   
![imagen](Imagenes Actividad2/nueve.jpg "Añadir tabla y commit")  
10. Posicionamiento en la rama mi nombre  
![imagen](Imagenes Actividad2/diez.jpg "Posicionamiento")  
11. Escribir en el fichero daw.md módulos de segundo  
![imagen](Imagenes Actividad2/once.jpg "Modificando el fichero daw.md")  
12. Añadir los archivos y hacer un commit  
![imagen](Imagenes Actividad2/doce.jpg "Añadir archivos y commit")  
13. Posicionamiento en master y hacer merge con la rama  
![imagen](Imagenes Actividad2/trecea.jpg "Posicionamiento en la rama y merge")  
* Aparece el conflicto, lo indica en git al hacer en merge y al abrir el fichero te aparecen las líneas  
![imagen](Imagenes Actividad2/treceb.jpg "Aparición del conflicto")   
* Solucionando el conflicto modificando el fichero para que aparezca lo que quiero  
![imagen](Imagenes Actividad2/trecec.jpg "Solución conflicto")    
14. Arreglar el conflicto y hacer un commit  
* Aparece el conflicto, lo indica en git al hacer en merge y al abrir el fichero te aparecen las líneas   
![imagen](Imagenes Actividad2/treceb.jpg "Aparición del conflicto")   
* Solucionando el conflicto modificando el fichero para que aparezca lo que quiero   
![imagen](Imagenes Actividad2/trecec.jpg "Solución conflicto")     
![imagen](Imagenes Actividad2/catorce.jpg "Commit")      
15. Crear etiqueta     
![imagen](Imagenes Actividad2/quince.jpg "Creacion de un etiqueta")     
16. Borrar la rama       
![imagen](Imagenes Actividad2/dieciseis.jpg "Borrar rama de mi nombre")     
![imagen](Imagenes Actividad2/dieciseisb.jpg "Borrar la rama de mi nombre")      <!--No debería aparecer lo de la última imagen-->    
17. Último commit y subir todo al servidor     
![imagen](Imagenes Actividad2/ultimo.jpg "Último commit y subir todo al servidor")    
