# DESARROLLO DE APLICACIONES WEB
#### Módulos primer curso de DAW  
|			Siglas 	  		    |			 Nombre			  			 |
|:-----------------------------:|:--------------------------------------:|
|				S.I.		 	|		   Sistemas Informáticos		 |
|				B.D.		 	| 			Bases de Datos 				 |
|	 			L.M.   			|           Lengauje de Marcas			 |
|      		   F.O.L.           | 	Formación y Orientación Laboral	     | 
|				E.D.	 		|		Entornos de Desarrollo           |
|				Prg				| 			Programación				 |  

#### Módulos segundo curso de DAW  
|			Siglas 	  		    |			 Nombre			  			 |
|:-----------------------------:|:--------------------------------------:|
|				DIW		 		|		Diseño de Interfaces Web 		 |
|		 DAW		 			|Despliegue de Aplicaciones Web 		 |
|	 DWEC   					|Desarollo Web en Entorno Cliente 		 |
|      DWES              		| 	Desarollo Web en Entorno Servidor	 | 
|			EIE			 		|   Empresa e Iniciativa Emprendedora	 |  

